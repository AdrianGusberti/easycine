<?php

namespace Industria\Easycine\Controller\User;

use Industria\Easycine\Helper\HtmlRendererTrait;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LoginPage implements RequestHandlerInterface
{
    use HtmlRendererTrait;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $html = $this->setView('login/formulario.php', [
            'titulo' => 'Login'
        ]);

        return new Response(200, [], $html);
    }
}
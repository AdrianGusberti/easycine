<?php

namespace Industria\Easycine\Controller\User;

use Industria\Easycine\Entity\User;
use Industria\Easycine\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Login implements RequestHandlerInterface
{
    use FlashMessageTrait;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->userRepository = $entityManager
            ->getRepository(User::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $email = filter_var(
            $request->getParsedBody()['email'],
            FILTER_VALIDATE_EMAIL
        );

        $loginRedirect = new Response(302, ['Location' => '/login']);

        if (is_null($email) || $email === false) {
            $this->setMessage(
                'danger',
                'O e-mail digitado não é um e-mail válido.'
            );

            return $loginRedirect;
        }

        $senha = filter_input(
            INPUT_POST,
            'senha',
            FILTER_SANITIZE_STRING
        );

        /** @var User $user */
        $usuario = $this->userRepository
            ->findOneBy(['email' => $email]);

        if (is_null($usuario) || !$usuario->verifyPassword($senha)) {
            $this->setMessage('danger', 'E-mail ou senha inválidos');

            return $loginRedirect;
        }

        $_SESSION['logado'] = true;

        return new Response(302, ['Location' => '/listar-cursos']);
    }
}

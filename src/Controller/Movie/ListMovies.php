<?php

namespace Industria\Easycine\Controller\Movie;

use Industria\Easycine\Entity\Movie;
use Industria\Easycine\Helper\HtmlRendererTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListMovies implements RequestHandlerInterface
{
    use HtmlRendererTrait;

    private $movieRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->movieRepository = $entityManager
            ->getRepository(Movie::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $html = $this->setView('cursos/listar-cursos.php', [
            'cursos' => $this->movieRepository->findAll(),
            'titulo' => 'Lista de cursos',
        ]);

        return new Response(200, [], $html);
    }
}

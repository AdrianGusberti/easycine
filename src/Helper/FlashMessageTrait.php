<?php

namespace Industria\Easycine\Helper;

trait FlashMessageTrait
{
    public function setMessage(string $type, string $message): void
    {
        $_SESSION['message_type'] = $type;
        $_SESSION['message'] = $message;
    }
}

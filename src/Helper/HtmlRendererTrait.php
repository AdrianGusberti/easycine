<?php

namespace Industria\Easycine\Helper;

trait HtmlRendererTrait
{
    public function setView(string $templatePath, array $data): string
    {
        extract($data);
        ob_start();

        require __DIR__ . '/../../view/header.php';
        require __DIR__ . '/../../view/' . $templatePath;
        require __DIR__ . '/../../view/footer.php';

        $html = ob_get_clean();

        return $html;
    }
}
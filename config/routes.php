<?php

use Industria\Easycine\Controller\{
    Movie\ListMovies,
    User\LoginPage,
    User\Login,
    User\Logout,
};

return [
    '/listar-cursos' => ListMovies::class,
    '/login-page' => LoginPage::class,
    '/login' => Login::class,
    '/logout' => Logout::class
];

